<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper functions for course_overview block
 *
 * @package    block_my_course_links
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Return sorted list of user courses
 *
 * @return array list of sorted courses grouped by term.
 */
function block_my_course_links_get_sorted_courses() {
    global $USER, $DB, $CFG;

    $courses = enrol_get_my_courses('id, shortname, fullname', 'fullname', 0);
    $site = get_site();

    if (array_key_exists($site->id,$courses)) {
        unset($courses[$site->id]);
    }

    /*
        Set the last access time on the course from the user object
    */
    foreach ($courses as $c) {
        if (isset($USER->lastcourseaccess[$c->id])) {
            $courses[$c->id]->lastaccess = $USER->lastcourseaccess[$c->id];
        } else {
            $courses[$c->id]->lastaccess = 0;
        }
    }

    // Find the termcode for each course
    if (!empty($courses)) {
        $courseids = array();
        foreach ($courses as $course) {
            $courseids[] = $course->id;
        }
        list ($insql, $inparams) = $DB->get_in_or_equal($courseids);
        $select = "enrol='wisc' AND courseid $insql";
        $enrols = $DB->get_records_select('enrol', $select, $inparams, '', 'id,courseid,customchar1');
        foreach ($enrols as $enrol) {
            if (empty($courses[$enrol->courseid]->term) || $courses[$enrol->courseid]->term < $enrol->customchar1) {
                $courses[$enrol->courseid]->term = $enrol->customchar1;
            }
        }
    }

    // get external courses
    require_once($CFG->dirroot . '/enrol/wisc/lib/externalcourses/external_user_courses.php');
    $externalcourses = new external_user_courses();
    $ecourses = $externalcourses->get_user_courses($USER->username, $courses);

    $courses = array_merge($courses, $ecourses);


    return array($courses, $externalcourses->get_errors());
}

/**
 * Group courses by term
 *
 * @param array $courses user courses
 * @return array
 */
function block_my_course_links_group_courses_by_term($courses) {
    global $DB;



    // Organize courses into terms, maintaining existing sorting inside each term
    $terms = array();
    foreach ($courses as $course) {
        if (!empty($course->term)) {
            $terms[$course->term][$course->id] = $course;
        } else {
            $terms[block_my_course_links::TERM_OTHER][$course->id] = $course;
        }
    }


    return $terms;
}

/**
 * Sort term array by ascending date
 *
 * @param array $terms keys are term_codes
 * @return boolean
 */
function block_my_course_links_sort_term_array(&$terms) {

    // Sort the terms with ongoing (code = 0) first, then in decreasing order
    $cmp = function ($a, $b) {
        if ($a == 0 || $b == 0) {
            return $a-$b;
        } else {
            return $b-$a;
        }
    };

    return uksort($terms, $cmp);
}

/**
 * Get the current term from our settings
 *
 * @return string termcode
 */
function block_my_course_links_get_current_term() {
    if (class_exists('\enrol_wisc\local\chub\timetable_util')) {
        return \enrol_wisc\local\chub\timetable_util::get_current_termcode();
    } else {
        return 0;
    }
}

/**
 * Return a string representing the term (e.g. "Fall 2010")
 * This function doesn't make any remote calls.
 *
 * @param string $termCode
 * @return string $termName
 */
function block_my_course_links_get_term_name($termCode) {
    $termCode = (string)$termCode;

    $c = substr($termCode,0,1);
    $yy = substr($termCode,1,2);
    $year = 1900+100*$c+$yy;
    $semester = substr($termCode,3,1);
    switch($semester) {
        case 2:
            $name = sprintf("Fall %d", $year-1);
            break;
        case 3:
            $name = sprintf("Winter %d", $year);
            break;
        case 4:
            $name = sprintf("Spring %d", $year);
            break;
        case 6:
            $name = sprintf("Summer %d", $year);
            break;
        default:
            $name = "Ongoing courses";
    }
    return $name;
}


/**
 * Builds course category tree from the courses array, which creates
 * a categoryid keyed array.
 *
 * This also returns an array of categoryids
 *
 * @param array $courses
 * @param int $blockinstancecontext The parent block instance context
 *
 * @return array of vars to send back
 */
function block_my_course_links_get_setup($courses, $blockinstancecontext){

    $tree = array();
    $categoryids = array();
    $selectedcourse = '';
    $selectedcategory = '';

    foreach($courses as $course){
        //get categoryids to get the category records from the category table later
        if(!in_array($course->category, $categoryids)){
            $categoryids[] = $course->category;
        }
        //build the tree out keyed by category and then keyed by course id
        if(isset($tree[$course->category])){
            $tree[$course->category][$course->id] = $course;
        }else{
            $tree[$course->category] = array();
            $tree[$course->category][$course->id] = $course;
        }

        if(context_course::instance($course->id)->id == $blockinstancecontext){
            $selectedcourse = $course->id;
            $selectedcategory = $course->category;
        }

    }

    return array($tree, $categoryids, $selectedcourse, $selectedcategory);
}


