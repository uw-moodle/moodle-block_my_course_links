<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * My Course Links block plugin installation script
 *
 * @package    block
 * @subpackage my_course_Links
 * @author     2013 John hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function xmldb_block_my_course_links_install() {
    global $CFG;

    if(check_dir_exists($CFG->dirroot . '/enrol/wisc', false)){
        set_config('usewisc', 1, 'block_my_course_links');
    }else{
        set_config('usewisc', 0, 'block_my_course_links');
    }
}

