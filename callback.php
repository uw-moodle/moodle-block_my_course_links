<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper functions for my_course_links block
 *
 * @package    block_my_course_links
 * @author	   2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");

require_login();

if(confirm_sesskey()) {
	$sectionName = required_param('section', PARAM_ALPHANUMEXT);
	$open = required_param('open', PARAM_ALPHA);

	if(!is_array($SESSION->mycourselinksblock) ){
		$SESSION->mycourselinksblock = array();
	}
	$SESSION->mycourselinksblock[$sectionName] =  ($open == 'true') ? 'true' : 'false';

    echo "Section: " . $sectionName;
    echo "<br />Open: " . $open;
    echo "<br /><pre>" . print_r($SESSION->mycourselinksblock, true) . "</pre>";
	echo 1;
	exit;
}
echo 0;


