

function mcl_create_warning_list(warnings){

    if(warnings.length == 0){
        return ''; // return nothing for no errors array
    }

    var returnHTML = '<ul>';
    $.each(warnings, function(index, value){
        returnHTML += '<li>' + value + '</li>';
    });

    returnHTML += '</ul>';
    return returnHTML;
}

/**
 * Perform DOM manipulations and send the change back to the callback for session
 *
 * @param section
 */
function toggleSection(section) {
    if($('#' + section + '-section-content').is(":visible")){
        $('#' + section + '-section-content').hide();
        $('#' + section + '-section-header-img').attr('src', my_course_links_plusicon);
        $.post(M.cfg.wwwroot +'/blocks/my_course_links/callback.php', { section: section, open: 'false', sesskey: M.cfg.sesskey});
    }else{
        $('#' + section + '-section-content').show();
        $('#' + section + '-section-header-img').attr('src', my_course_links_minusicon);
        $.post(M.cfg.wwwroot +'/blocks/my_course_links/callback.php', { section: section, open: 'true', sesskey: M.cfg.sesskey});
    }
}

function opensections(){

    for(var x = 0; x < my_course_links_options.length; x++){
        var option = my_course_links_options[x];

        if(option.open == 'false'){
            // Check to make sure that the element exists before attempting to do something
            if($('#' + option.sectionName + '-section-content').exists()){

                $('#' + option.sectionName + '-section-content').hide();
                $('#' + option.sectionName + '-section-header-img').attr('src', my_course_links_plusicon);
            }
        }
    }
}


function get_section_content(section, selectedcourseid){

    var returnHTML = '<div id="' + section.termcode + '-section" class="box my-course-links-section">';

    // build header part
    returnHTML += '<div id="' + section.termcode + '-section-header" class="my-course-links-sectionheader" ' +
                    'onclick="toggleSection(\'' + section.termcode + '\');">';
        // Indented to help with reading of the HTML being built
        returnHTML += '<img class="my-course-links-control" id="' + section.termcode + '-section-header-img" src="' + window.my_course_links_minusicon +
                        '" alt="minusicon" />';
        returnHTML += '<span class="my-course-links-headertext">' + section.name + '</span>';
    returnHTML += '</div>';

    // next build section content
    returnHTML += '<div id="' + section.termcode + '-section-content" class="my-course-links-sectioncontent">';

        // Indented to help with reading of HTML being built
        returnHTML += '<ul class="my-course-links-section-list">';

        $.each(section.courses, function(index, course){

            var selected = '';
            if(course.id === selectedcourseid){
                selected = ' my-course-links-selected';
            }

            var externalcourse = ' internal-course';
            if(course.externalcourse === 1){
                externalcourse = ' external-course';
            }

            returnHTML += '<li class="my-course-links-course' + selected + '">';
            //returnHTML += '<div class="my-course-links-' + course.lms + '-icon"></div>';
            returnHTML += '<div class="my-course-links-' + course.lms + '-course' + externalcourse + '">';
                returnHTML += '<a href="' + course.url + '">' + course.name + '</a>';
            returnHTML += '</div>';
            returnHTML += '</li>';
        });

        returnHTML += '</ul>';
    returnHTML += '</div>';

    returnHTML += '</div>'; // end section container div

    return returnHTML;
}



// Extend jQuery to add the method exists
$.fn.exists = function() {
    return this.length > 0;
}

$(function(){

    if(window.my_course_links_useajax == 'true'){
        $.post(M.cfg.wwwroot + '/blocks/my_course_links/getcourses.php',
                {sesskey: M.cfg.sesskey, pcontext: my_course_links_pcontext},
        function(response){

            var content = ''; // holds content to display in the block

            // setup current term courses first
            if(response.currentterm.courses.length > 0){ // add it if there are courses
                content += get_section_content(response.currentterm, response.selectedcourseid);
            }

            // next do other term courses
            if(response.otherterms.length > 0){
                $.each(response.otherterms, function(index, otherterm){
                   content+= get_section_content(otherterm, response.selectedcourseid);
                });
            }

            // finally do ongoing courses
            if(response.ongoing.courses.length > 0){
                content += get_section_content(response.ongoing, response.selectedcourseid);
            }

            document.getElementById('my_course_links_content').innerHTML = content;

            window.my_course_links_options = response.blockoptions;

            opensections();

            if(response.errors !== undefined){

                var warningMsg = '';
                if( Object.prototype.toString.call( response.errors ) === '[object Array]' ) {
                    // we have a list of errors
                    warningMsg = mcl_create_warning_list(response.errors);

                }else{
                    // we have a general warning string
                    warningMsg = response.errors;
                }

                if(warningMsg.length > 0){ // if there is a warning message show it
                    document.getElementById('warning_container').innerHTML = warningMsg;
                    $('#warning_area').show();
                }
            }

        }, 'json');
    }else{
        opensections();
    }

});

