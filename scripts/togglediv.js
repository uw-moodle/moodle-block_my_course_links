
/**
 * Perform DOM manipulations and send the change back to the callback for session
 *
 * @param section
 */
function toggleSection(section) {
    if($('#' + section + '-section-content').is(":visible")){
        $('#' + section + '-section-content').hide();
        $('#' + section + '-section-header-img').attr('src', my_course_links_plusicon);
        $.post(my_course_links_wwwroot+'/blocks/my_course_links/callback.php', { section: section, open: 'false', sesskey: my_course_links_sesskey});
    }else{
        $('#' + section + '-section-content').show();
        $('#' + section + '-section-header-img').attr('src', my_course_links_minusicon);
        $.post(my_course_links_wwwroot+'/blocks/my_course_links/callback.php', { section: section, open: 'true', sesskey: my_course_links_sesskey});
    }
}



// Extend jQuery to add the method exists
$.fn.exists = function() {
    return this.length > 0;
}

$(function(){

    for(var x = 0; x < my_course_links_options.length; x++){
        var option = my_course_links_options[x];

        if(option.open == 'false'){
            // Check to make sure that the element exists before attempting to do something
            if($('#' + option.sectionName + '-section-content').exists()){

                $('#' + option.sectionName + '-section-content').hide();
                $('#' + option.sectionName + '-section-header-img').attr('src', my_course_links_plusicon);
            }
        }
    }
});

