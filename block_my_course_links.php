<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * EPD Course overview block for UW Moodle
 *
 * This block uses much of the same features as the course_overview_uwmoodle block,
     but is tailored to the EPD requests for certain features
 *
 * The block is dependent on the UW enrollment plugins and is largely coped from the existing course_overview block.
 *
 * @author John Hoopes / Matt Petro
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/blocks/my_course_links/locallib.php');
require_once($CFG->dirroot.'/blocks/moodleblock.class.php');

/**
 *  This is the main block class for the my_course_links block
 * @package my_course_links
 * @author John Hoopes <hoopes@wisc.edu>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_my_course_links extends block_base {

    // our "term_code" for non-timetable classes
    const TERM_OTHER = 0;

    /** @var int $usewisc Determine whether or not to use the wisc plugin */
    protected $usewisc;

    /**
     * Set the title of the block in the initializing function
     *
     * @global object $USER Defined by Moodle
     * @global object $CFG
     * @global object $DB
     */
    public function init() {
        global $USER, $CFG, $DB;
        $this->title = get_string('pluginname', 'block_my_course_links');
    }

    /**
     * Set up required page javascripts
     * Also calls parent as well
     */
    public function get_required_javascript(){
        global $CFG;

        parent::get_required_javascript();

        if($CFG->version >= '2013051400'){
            $this->page->requires->jquery();
        }
    }


    /**
     * Defines the content for the block
     *
     * Processes the current courses for the user and then puts them into
     * distinct terms. These terms are then rendered to the page using
     * the custom renderer.
     *
     * @return mixed Returns a newly formed HTML string or already existing content
     */
    public function get_content() {
        global $USER, $CFG, $DB;

        if($this->content !== null) {
            return $this->content;
        }
        $open = microtime(true);

        $this->usewisc = get_config('block_my_course_links', 'usewisc');
        if($this->usewisc){
            $this->setup_wisc_block();
        }else{
            $this->setup_default();
        }


        //$this->content->footer	= 'Generated: ' . round(microtime(true) - $open, 4) . 's';
        return $this->content;
    }

    /**
     * Sets up the block to work without the enrol_wisc plugin
     *
     * This will create a list of courses the user is enrolled in
     *
     */
    protected function setup_default(){
        global $USER, $CFG, $DB;

        // set up the content and get the renderer for this block
        $renderer = $this->page->get_renderer('block_my_course_links');
        $this->content			= new stdClass;
        $this->content->text	= '';

        $courses = enrol_get_users_courses($USER->id, true);
        $allcourses = enrol_get_all_users_courses($USER->id);

        if(!empty($courses)){

            //get the setup vars for this block instance
            // doing all of this in one function limits the number of times looping through the courses var
            list($tree, $categoryids, $selectedcourse, $selectedcategory) = block_my_course_links_get_setup($courses,
                                                                                        $this->instance->parentcontextid);

            //Get the categories from the courses
            list($insql, $params) = $DB->get_in_or_equal($categoryids);
            $sql = 'SELECT * FROM {course_categories} WHERE id ' . $insql;
            $categories = $DB->get_records_sql($sql, $params);

            foreach($tree as $categoryid => $tcategory){
                $asteriskHeader = false;
                foreach($tcategory as $tcourse){
                    $this->get_unread_forum_post_count($tcourse);
                    if(isset($tcourse->unreadPosts)){
                        if($tcourse->unreadPosts > 0){
                            $asteriskHeader = true;
                        }
                    }
                }

                $this->content->text .= $renderer->render_section($categories[$categoryid]->name, $tcategory,
                                                                            $selectedcourse, $asteriskHeader);
            }
            $this->content->text   .= $this->addBlockScript(false, null, $selectedcategory, $categories);
            $this->content->footer = $renderer->renderfooter();
        }

    }


    /**
     * Sets up the block as the UW Moodle version of this block
     *
     * This includes grouping courses by semesters for the student and uses the enrol_wisc plugin
     *
     */
    protected function setup_wisc_block(){
        global $USER, $CFG, $DB;


        // set up the content and get the renderer for this block
        $renderer = $this->page->get_renderer('block_my_course_links');
        $this->content = new stdClass;
        $this->content->text = '';

        $usephp = optional_param('usephp', '', PARAM_ALPHA);
        if(empty($usephp)){ // use the ajax route

            $this->content->text .= $renderer->render_error_container($usephp);

            $this->content->text .= $renderer->setup_ajax();
            // add basic block script here.  the options for open/closed will be gotten from the ajax callback
            $this->content->text .= $this->addBlockScript(true);

        }else {
            // set up the courses and terms into arrays as well as getting the current term courses
            list($courses, $errors) = block_my_course_links_get_sorted_courses();
            $terms = block_my_course_links_group_courses_by_term($courses);
            $currentterm = block_my_course_links_get_current_term();

            $this->content->text .= $renderer->render_error_container($usephp, $errors);

            if(!isset($terms[$currentterm])) {
                $terms[$currentterm] = array();
            }
            if(!isset($terms[self::TERM_OTHER])) {
                $terms[self::TERM_OTHER] = array();
            }

            // Sort the terms with newest first
            block_my_course_links_sort_term_array($terms);

            $selectedcourse = '';
            $selectedcourseterm = '';
            $termList = array();
            foreach($terms as $key => $term) {
                array_push($termList, $key);
                foreach($term as $course) { // a term is an array of courses
                    if(get_class($course) != 'external_course'){
                        if(context_course::instance($course->id)->id == $this->instance->parentcontextid) {
                            $selectedcourse = $course->id;
                            $selectedcourseterm = $key;
                        }
                    }
                }
            }

            // Render the current term first, followed by the other terms. and then lastly by other courses
            // Only show the first term if there are courses in it.
            if(count($terms[$currentterm]) > 0) {
                $termName = block_my_course_links_get_term_name($currentterm);
                $asteriskHeader = false;
                foreach($terms[$currentterm] as $course) {
                    //$this->get_unread_forum_post_count($course); // unread forum posts are disabled for now
                    if(isset($course->unreadPosts)) {
                        if($course->unreadPosts > 0) {
                            $asteriskHeader = true;
                        }
                    }
                }
                $this->content->text .= $renderer->render_section($currentterm, $termName, $terms[$currentterm], $selectedcourse, $asteriskHeader);
            }
            unset($terms[$currentterm]); // unset the current term

            // store the other term for later and then unset from main array
            $otherTerm = $terms[self::TERM_OTHER];
            unset($terms[self::TERM_OTHER]);

            // loop through other terms if there are any and render them
            foreach($terms as $termCode => $term) {
                if(count($term) > 0) { // if there are actually courses
                    $termName = block_my_course_links_get_term_name($termCode);
                    $asteriskHeader = false;
                    foreach($term as $course) {
                        //$this->get_unread_forum_post_count($course); // unread forum posts are disabled for now
                        if(isset($course->unreadPosts)) {
                            if($course->unreadPosts > 0) {
                                $asteriskHeader = true;
                            }
                        }
                    }
                    $this->content->text .= $renderer->render_section($termCode, $termName, $term, $selectedcourse, $asteriskHeader);
                }
            }
            // Lastly render the other terms section
            if(count($otherTerm) > 0) {
                $asteriskHeader = false;
                foreach($otherTerm as $course) {
                    //$this->get_unread_forum_post_count($course); // unread forum posts are disabled for now
                    if(isset($course->unreadPosts)) {
                        if($course->unreadPosts > 0) {
                            $asteriskHeader = true;
                        }
                    }
                }
                $this->content->text .= $renderer->render_section(0, 'Ongoing courses', $otherTerm, $selectedcourse, $asteriskHeader);
            }
            $this->content->text .= $this->addBlockScript(false, $currentterm, $selectedcourseterm, $termList);

        }

        $this->content->footer = $renderer->renderfooter();

    }


    /**
     * Adds in the javascript processing for this block.
     *
     * By having javascript processing the opening and closing of the block sections
     * we can allow for a graceful degradation of the block if there is no-js
     *
     * User actions on opening/closing sections are "saved" in the session and are then
     * defined in the javascript configuration output.
     *
     * @param bool $useajax Whether or not we are using ajax. This defines the behavior of this function
     * @param int $currentterm Holds the integer key of the current term
     * @param int $selectedsection Holds the section key that the potential selected course is in (term or category)
     * @param array $sections holds an array of sections (be that terms or categories) that this user is in
     *
     * @return string Returns a string of html/javascript in order to provide configuration
     */
    public function addBlockScript($useajax = false, $currentterm = null, $selectedsection = null, $sections = array()) {
        global $CFG, $SESSION, $USER;

        //build an array for javascript to interpret to open/close right sections on ready
        //	This allows for the graceful degradation of the block with no javascript

        //Set a new instance of mycourselinksblock in the session if it doesn't exist
        if(!isset($SESSION->mycourselinksblock)){
            $this->resetSession();
        }
        if(!is_array($SESSION->mycourselinksblock)){
            $this->resetSession();
        }

        $arrOptions = array();
        foreach($sections as $section){
            $option = new stdClass();
            $option->sectionName = $section;

            $option->open = $this->get_open_default($section, $currentterm, $selectedsection);

            //give the session a chance to change the option option based on user interface selections
            // and reported to the session through callback.php
            if( isset($SESSION->mycourselinksblock[$option->sectionName]) ) {
                $option->open = $SESSION->mycourselinksblock[$option->sectionName];
            }
            array_push($arrOptions, $option);
        }

        $html = '
        <script type="text/javascript">
            var my_course_links_useajax = "' . ( ($useajax) ? 'true' : 'false' ) .'";
            var my_course_links_pcontext = "' . $this->instance->parentcontextid . '";
            var my_course_links_plusicon = "' . $CFG->wwwroot . '/blocks/my_course_links/pix/plus.gif";
            var my_course_links_minusicon = "' . $CFG->wwwroot . '/blocks/my_course_links/pix/minus.gif";
            var my_course_links_options = ' . json_encode($arrOptions) . ';
        </script>
        ';
        $html .= '<script src="' . $CFG->wwwroot . '/blocks/my_course_links/scripts/my_course_links.js"></script>';

        return $html;
    }

    /**
     * Gets the section name based upon the config option
     * of whether or now wisc enrollments is used
     *
     * @param mixed $section
     *
     * @return string
     */
    protected function get_section_name($section){

        if($this->usewisc){
            return block_my_course_links_get_term_name($section);
        }else{
            return $section->name;
        }
    }

    /**
     * Sets the open option default depending on the whether or not usewisc is true or false
     *
     * @param mixed $section
     * @param int|null $currentterm
     * @param int $selectedsection
     *
     * @return string string version of true or false
     */
    protected function get_open_default($section, $currentterm, $selectedsection){

        if($section == $currentterm){
            return 'true';
        }else if($section == $selectedsection && strlen($selectedsection) > 0){
            return 'true';
        }else{
            return 'false';
        }
    }

    /**
     * Reset the session mycourselinksblock var if it isn't set correctly
     *
     * @return mixed There is no return as the session is just modified
     */
    public function resetSession(){
        global $SESSION;

        $SESSION->mycourselinksblock = array();
    }

    /**
     * Determines the number of unread forum posts for the course.
     *
     * The user must have the user preference of "Forum Tracking" to
     *  "Yes: highlight new posts for me".  Otherwise forum_tp_can_track_forums
     *  will return false and there will be no forum post count on the course link
     *
     * @param object $course - passed in by reference to set the unread count on the course var itself instead
     *  of adding it in the get_content function
     * @return bool Returns false when user does not track forums
     */
    public function get_unread_forum_post_count(&$course){
        global $CFG, $USER, $DB;

        require_once($CFG->dirroot . '/mod/forum/lib.php');
        if( forum_tp_can_track_forums() ){ //if the user can track forum posts as a setting, try to find unread forum posts
            //next see which forums are un-tracked by the user and in the specific course
            $untracked = forum_tp_get_untracked_forums($USER->id, $course->id);

            //get the forum instances for the course
            $results = $DB->get_records_sql('SELECT cm.* FROM {course_modules} AS cm
                INNER JOIN {course} AS course ON course.id = cm.course
                INNER JOIN {modules} AS modules ON cm.module = modules.id
                WHERE
                    modules.name = :modname AND
                    cm.visible = :visible AND
                    cm.course = :course',
                array(
                    'modname'=>'forum',
                    'visible'=>'1',
                    'course'=>$course->id,
                )
            );
            $unreadPosts = 0;
            foreach($results as $cm){
                if(!isset($untracked[$cm->instance])) {
                    $unreadPosts += forum_tp_count_forum_unread_posts($cm, $course);
                }
            }
            $course->unreadPosts = $unreadPosts;
        }else{
            return false;
        }
    }

    /**
     * allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Block cron to update currentterm
     *
     * @return boolean true on success
     */
    public function cron() {
        global $CFG;

        if(check_dir_exists($CFG->dirroot . '/enrol/wisc', false)){
            set_config('usewisc', 1, 'block_my_course_links');
        }else{
            set_config('usewisc', 0, 'block_my_course_links');
            return true;
        }
    }


}






