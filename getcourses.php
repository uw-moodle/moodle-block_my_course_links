<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax callback for the course_overview_uwmoodle block
 * Gets the user's courses and returns it to the javascript in the page
 *
 * @package    block_my_course_links
 * @author	   2014 John Hoopes
 * @copyright  University of Wisconsin System - Board of Regents
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// define that this is an ajax script
define('AJAX_SCRIPT', 1);

require_once("../../config.php");

require_login();

$response = new stdClass();
$response->status = 'false';
$response->message = 'invalidrequest';

$PAGE->set_context(context_system::instance());

if(confirm_sesskey()) {

    // now require the block lib files
    require_once($CFG->dirroot.'/blocks/my_course_links/block_my_course_links.php');
    global $SITE;
    $parentcontext = optional_param('pcontext', context_course::instance(SITEID), PARAM_INT);

    // set up the courses and terms into arrays as well as getting the current term courses
    list($courses, $errors) = block_my_course_links_get_sorted_courses();
    $terms = block_my_course_links_group_courses_by_term($courses);
    $currentterm = block_my_course_links_get_current_term();

    if(!empty($errors)){
        if(debugging() && is_siteadmin()){
            $response->errors = $errors;
        }else{
            $response->errors = get_string('general_warning_message', 'block_my_course_links');
        }
    }

    if(!isset($terms[$currentterm])) {
        $terms[$currentterm] = array();
    }
    if(!isset($terms[block_my_course_links::TERM_OTHER])) {
        $terms[block_my_course_links::TERM_OTHER] = array();
    }

    // Sort the terms with newest first
    block_my_course_links_sort_term_array($terms);

    $selectedcourse = '';
    $selectedcourseterm = '';
    $termList = array();
    foreach($terms as $key => $term) {
        array_push($termList, $key);
        foreach($term as $course) { // a term is an array of courses
            if(get_class($course) != 'external_course'){
                if(context_course::instance($course->id)->id == $parentcontext) {
                    $selectedcourse = $course->id;
                    $selectedcourseterm = $key;
                }
            }
        }
    }

    // store the ongoing for later and then unset from main array
    $ongoing = $terms[block_my_course_links::TERM_OTHER];
    unset($terms[block_my_course_links::TERM_OTHER]);

    // get current term.
    $response->currentterm = new stdClass();
    $response->currentterm->name = '';
    $response->currentterm->courses = array();
    if(count($terms[$currentterm]) > 0) {
        $termName = block_my_course_links_get_term_name($currentterm);

        $response->currentterm->name = $termName;
        $response->currentterm->termcode = $currentterm;

        // loop through current term courses to add the courses to the object
        $response->currentterm->courses = array();
        foreach($terms[$currentterm] as $course){
            $returncourse = new stdClass();
            $returncourse->id = $course->id;
            $returncourse->name = $course->fullname;
            if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                $courseurl = new moodle_url($course->courseurl);
                $returncourse->lms = $course->LMS;
                $returncourse->externalcourse = 1;
            }else{
                $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                $returncourse->lms = 'Moodle'; // local courses are always Moodle
                $returncourse->externalcourse = 0;
            }
            $returncourse->url = $courseurl->out();

            $response->currentterm->courses[] = $returncourse;
        }
    }
    unset($terms[$currentterm]); // unset the current term

    // loop through other terms if there are any and add them
    $response->otherterms = array();
    foreach($terms as $termCode => $term) {
        if(count($term) > 0) { // if there are actually courses

            $responseTerm = new stdClass();
            $responseTerm->name = block_my_course_links_get_term_name($termCode);
            $responseTerm->termcode = $termCode;
            // loop through current term courses to add the courses to the object
            $responseTerm->courses = array();
            foreach($term as $course){
                $returncourse = new stdClass();
                $returncourse->id = $course->id;
                $returncourse->name = $course->fullname;
                if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                    $courseurl = new moodle_url($course->courseurl);
                    $returncourse->lms = $course->LMS;
                    $returncourse->externalcourse = 1;
                }else{
                    $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                    $returncourse->lms = 'Moodle'; // local courses are always Moodle
                    $returncourse->externalcourse = 0;
                }
                $returncourse->url = $courseurl->out();

                $responseTerm->courses[] = $returncourse;
            }

            $response->otherterms[] = $responseTerm;
        }
    }
    // Lastly add the other terms section
    $response->ongoing = new stdClass();
    $response->ongoing->name = '';
    $response->ongoing->courses = array();
    if(count($ongoing) > 0) {
        $asteriskHeader = false;

        $response->ongoing->name = 'Ongoing courses';
        $response->ongoing->termcode = 0;

        // loop through current term courses to add the courses to the object
        $response->ongoing->courses = array();
        foreach($ongoing as $course){
            $returncourse = new stdClass();
            $returncourse->id = $course->id;
            $returncourse->name = $course->fullname;
            if(get_class($course) == 'external_course'){ // external courses have their urls already defined
                $courseurl = new moodle_url($course->courseurl);
                $returncourse->lms = $course->LMS;
                $returncourse->externalcourse = 1;
            }else{
                $courseurl = new moodle_url('/course/view.php', array('id' => $course->id));
                $returncourse->lms = 'Moodle'; // local courses are always Moodle
                $returncourse->externalcourse = 0;
            }
            $returncourse->url = $courseurl->out();


            $response->ongoing->courses[] = $returncourse;
        }
    }

    global $SESSION;
    $arrOptions = array();
    foreach($termList as $section){
        $option = new stdClass();
        $option->sectionName = $section;


        if($section == $currentterm){
            $option->open = 'true';
        }else if($section == $selectedcourseterm && strlen($selectedcourseterm) > 0){
            $option->open = 'true';
        }else{
            $option->open = 'false';
        }

        //give the session a chance to change the option option based on user interface selections
        // and reported to the session through callback.php
        if( isset($SESSION->mycourselinksblock[$option->sectionName]) ) {
            $option->open = $SESSION->mycourselinksblock[$option->sectionName];
        }
        array_push($arrOptions, $option);
    }

    $response->status = 'true';
    $response->message = 'success';
    $response->selectedterm = $selectedcourseterm;
    $response->selectedcourseid = $selectedcourse;
    $response->blockoptions = $arrOptions;
    $response->nocoursesmessage = get_string('nocourses','block_course_overview_uwmoodle');
}


echo json_encode($response);


