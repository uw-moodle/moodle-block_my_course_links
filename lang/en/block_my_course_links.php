<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * EPD Course overview block for UW Moodle
 *
 * @author John Hoopes / Matt Petro
 */

defined('MOODLE_INTERNAL') || die();

//$string['pluginname'] = 'Course Connections';
$string['pluginname'] = 'My course links';
$string['shortname'] = 'Course links';
$string['my_course_links:addinstance'] = 'Add a new My course links block';
$string['my_course_links:myaddinstance'] = 'Add a new My course links block to the My Moodle page';

$string['moodle_legend'] = '- Moodle';
$string['d2l_legend'] = '- D2L';

$string['nojavascript'] = 'Javascript is not enabled on this page.  Click this link to load the block';

$string['warning_header'] = 'Warning: Partial list of courses';
$string['general_warning_message'] = "We're sorry, something went wrong when we tried to load all of your classes.  " .
    "If you continue to experience issues, contact the DoIT Help Desk at 264-HELP (4357).";


