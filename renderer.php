<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

/**
 *  Custom render class for the my_course_links block
 *
 * @package my_course_links
 * @author John Hoopes <hoopes@wisc.edu>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_my_course_links_renderer extends plugin_renderer_base {

    /** @var string $title holds the actual section name without underscores for display */
 	protected $title;

    /**
     * Main Render Function
     *
     * Builds the HTMl for one of the sections(terms) in the block as it acts as the aggregator for
     * each of the separate functions in this class
     *
     * @param int $termCode The termcode of the section we are rendering.  is used for div ids
     * @param string $sectionName The Name of the Section
     * @param array $courses The courses that go into this section
     * @param int $selectedCourse The course ID of the Selected Course
     * @param bool $asteriskHeader Determines whether or not this section is asterisked
     *
     * @return string The section html
     */
	public function render_section($termCode, $sectionName, $courses, $selectedCourse, $asteriskHeader) {

		//Replace any spaces in the section name with underscores
		$this->title = $sectionName;
		$htmlid = $termCode; // just to be explicit readability
		//The section name variable will now be used for IDs and styling

		$sectionhtml = $this->output->box_start('my-course-links-section', $htmlid . '-section');

		$sectionhtml .= $this->build_clickable_header($htmlid, $asteriskHeader);
		$sectionhtml .= $this->build_section_content($htmlid, $courses, $selectedCourse);

		$sectionhtml .= $this->output->box_end();

		return $sectionhtml;
	}

    /**
     * Builds the header
     *
     * Builds the header for the course section, but is clickable in order to fold
     * the entire section
     *
     * @param string $htmlid The termcode of the section
     * @param bool $asteriskHeader Whether or not to put an asterisk on the header name
     *
     * @return string The html for the header part of the section
     */
	public function build_clickable_header($htmlid, $asteriskHeader) {
		global $CFG;

		$html = html_writer::start_tag('div', array('id' => $htmlid . '-section-header', 'class' => 'my-course-links-sectionheader', 'onclick' => 'toggleSection(\'' . $htmlid . '\');') );


		$imgSRC = $CFG->wwwroot . '/blocks/my_course_links/pix/minus.gif';
		$alt = 'minus';

		$html .= html_writer::empty_tag('img', array('class' => 'my-course-links-control', 'id' => $htmlid . '-section-header-img', 'src' => $imgSRC, 'alt' =>$alt) );

		//set the section name
		$html .= html_writer::start_tag('span', array('class' => 'my-course-links-headertext'));
		$html .= $this->title;
		if($asteriskHeader){
			$html .= '* ';
		}
		$html .= html_writer::end_tag('span');

		//end header section
		$html .= html_writer::end_tag('div');

		return $html;
	}

    /**
     * Builds the section content
     *
     * Builds the section content with the courses and highlights (bolds) a course if it
     * is the selected course.
     *
     * @param string $htmlid The termcode of the section
     * @param array $courses The array of courses for this section
     * @param int $selectedCourse The course id for the possibly selected course
     *
     * @return string The html for the content part of the section
     */
	public function build_section_content($htmlid, $courses, $selectedCourse) {
		global $CFG;
        // start the content div section
		$html = html_writer::start_tag('div', array('id' => $htmlid . '-section-content', 'class' => 'my-course-links-sectioncontent') );
		$html .= html_writer::start_tag('ul', array('class' => 'my-course-links-section-list'));
		foreach($courses as $course){ // loop through each course to add it to the un ordered list

            if($selectedCourse == $course->id){
                $sClass = 'my-course-links-selected my-course-links-course';
            }else{
                $sClass = 'my-course-links-course';
            }

			$html .= html_writer::start_tag('li', array('class' => $sClass) );

			if(isset($course->unreadPosts)){
				if($course->unreadPosts == 0){
					$lText = $course->fullname;
				}else{
					$lText = $course->fullname . ' (' . $course->unreadPosts . ')' ;
				}
			}else{
				$lText = $course->fullname;
			}

            // set classes for div holding the link
            // this is for the LMS Icon
            if(get_class($course) == 'external_course'){
                $lcClass = 'my-course-links-' . $course->LMS . '-course external-course';
                $linkurl = $course->courseurl;
            }else{
                $lcClass = 'my-course-links-Moodle-course internal-course';
                $linkurl = $CFG->wwwroot . '/course/view.php?id=' . $course->id;
            }

			$link = html_writer::link($linkurl, $lText, array());
            $html .= html_writer::tag('div', $link, array('class' => $lcClass)); // surround link with a div for LMS icons

			$html .= html_writer::end_tag('li');

		}
		$html .= html_writer::end_tag('ul');
		$html .= html_writer::end_tag('div');

		return $html;
	}

    /**
     * Render a custom footer to tell users that if there is no javascript
     * the opening and closing will not work.
     *
     * @return string $html The html string for the footer
     */
    public function renderfooter(){
        $html = '';

        $html .= html_writer::tag('hr', '', array('class'=>'my-course-links-footer-rule'));
        $html .= html_writer::start_tag('div', array('class'=>'my-course-links-legend'));
        $html .= html_writer::tag('div', get_string('moodle_legend', 'block_my_course_links'), array('class'=>'my-course-links-Moodle-course'));
        //$html .= html_writer::tag('div', get_string('d2l_legend', 'block_my_course_links'), array('class'=>'my-course-links-D2L-course'));
        $html .= html_writer::end_tag('div');

        return $html;
    }

    /**
     * There are 2 instances in which this function is used
     *  -  When the block is functioning in AJAX mode and using the callback, all this function will do
     *      will return the html of the error container to let JS put the error message into the container
     *  -  When the block is functioning in PHP mode this function will recieve errors if there are any.  And if there are
     *      call error_log() on them to add them to the error log as well add them to the error container
     *
     * @param string $usephp Whether or not to show the section, as in php mode there is no js to show the block
     * @param array $errors An array of errors given by external courses
     * @return string HTML string of the error container with possible errors
     */
    public function render_error_container($usephp, $errors = array()){

        if(empty($usephp)){
            $showcontainer = 'hidecontainer';
        }else{

            if(empty($errors)){
                $showcontainer = 'hidecontainer';
            }else{
                $showcontainer = '';
            }
        }

        $output = $this->output->box_start('notification_area warning_bg ' . $showcontainer, 'warning_area' );
        $output .= html_writer::tag('h3', get_string('warning_header', 'block_my_course_links'), array('class'=>'notification_header warning_h'));

        $output .= html_writer::start_tag('div', array('class' => 'notifications_container', 'id'=>'warning_container'));

        if(!empty($errors)){

            foreach($errors as $error){
                error_log($error);
            }

            if(debugging() && is_siteadmin()){ // if debugging show actual error messages

                $output .= html_writer::alist($errors);

            }else{ // show general message
                $output .= get_string('general_warning_message', 'block_my_course_links');
            }

        }
        $output .= html_writer::end_tag('div');
        $output .= $this->output->box_end();
        return $output;
    }


    /**
     * function to set up the ajax functions
     *
     * @return string
     */
    public function setup_ajax(){
        global $PAGE;

        $html = '';

        $html .= html_writer::start_tag('div', array('class' => 'courselistcontainer', 'id' => 'my_course_links_content'));

        // add in loading courses and no javascript divs for display on the block when using ajax
        $html .= html_writer::tag('div', '', array('class'=>'loadingcourses'));
        $html .= html_writer::start_tag('div', array('class'=>'nojavascript'));
        $usephpurl = clone($PAGE->url);
        $usephpurl->param('usephp', 'true');
        $html .= html_writer::link($usephpurl, get_string('nojavascript', 'block_my_course_links'));
        $html .= html_writer::end_tag('div');


        $html .= html_writer::end_tag('div');

        return $html;

    }

}
